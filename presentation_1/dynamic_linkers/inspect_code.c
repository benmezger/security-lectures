#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>

typedef int (*orig_open_f_type)(const char *pathname, int flags);

int open(const char *pathname, int flags, ...){
    // let's inject our evil code here..
    printf("OPEN is accessing: %s\n", pathname);
}
