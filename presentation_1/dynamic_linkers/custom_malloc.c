#include <dlfcn.h>
#include <stdio.h>

// gcc -ldl -Wall -shared -fPIC -o custom_malloc.so -D_GNU_SOURCE custom_malloc.c

void *malloc(size_t size)
{
    static void * (*malloc_original)(size_t) = NULL;
    void * memory_pointer;

    if (malloc_original == NULL)
        malloc_original = (void *(*)()) dlsym(RTLD_NEXT, "malloc");

    memory_pointer = malloc_original(size);
    printf("malloc(%d) returns pointer %p\n", size, memory_pointer);     
    return memory_pointer;
}
